import request from "request";
import { config } from "../config"

export const getCountry = async (req): Promise<any> => {
    return new Promise((resolve, reject) => {
        const reqCountryOptions = {
            url: `${config.restCountries}name/${req.query.countryName}?fullText=true&fields=name;flag`,
            method: req.method
        };

        request(reqCountryOptions, function (error, message, body) {
            if (error) {
                return reject(error);
            }
            return resolve(JSON.parse(body));
        });
    });
}

export const getAllCountries = async (req): Promise<any> => {
    return new Promise((resolve, reject) => {
        const reqAllCountriesOptions = {
            url: `${config.restCountries}all?fields=name;alpha2Code;flag`,
            method: req.method
        };

        request(reqAllCountriesOptions, function (error, message, body) {
            if (error) {
                return reject(error);
            }
            return resolve(JSON.parse(body));
        });
    });
}

export const getCountriesListByCode = async (req, countryCodes): Promise<any> => {
    return new Promise((resolve, reject) => {
        const reqFilterCodeOptions = {
            url: `${config.restCountries}alpha?codes=${countryCodes}&fields=name;flag`,
            method: req.method
        };

        request(reqFilterCodeOptions, function (error, message, body) {
            if (error) {
                return reject(error);
            }
            return resolve(JSON.parse(body));
        });
    });
}