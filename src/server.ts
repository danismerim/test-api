import express from "express";
import cors from "cors";
import routes from "./routes/index";

const app = express();
app.use(express.json());
app.use(cors());

routes(app);

app.listen(3333, () => console.log("Server is running on port 3333"));