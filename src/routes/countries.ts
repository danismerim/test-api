import { getAllCountries, getCountriesListByCode, getCountry } from "../services/countries"

module.exports.addRoutes = (app) => {

    /**
     * Send a request from the frontend to the backend with a string ‘countryName’ which the backend 
     * will use to return a single matched country.
        ● On the backend, use this API for countries https://restcountries.eu/
        ● Display the country on the frontend
    */
    app.get("/countries/search", async (req, res) => {
        await getCountry(req)
            .then((country) => {
                if (country[0]) {
                    return res.json({ name: country[0].name, flag: country[0].flag });
                }
                return res.status(country.status).json({ message: country.message });
            })
            .catch((error) => {
                return res.status(error.status).json({ message: error.message });
            })
    })

    /**
     * Send a request from the frontend to the backend with an array of strings ‘countryNames’ 
     * which will return a list of countries partly matching any of the strings.
        ● On the backend, use this API for countries https://restcountries.eu/
        ● Display the countries on the frontend
    */
    app.get("/countries/search-list", async (req, res) => {

        /**
         * There is no endpoint on the restcountries api to search for a list by name, only by list of code.
            I wanted to avoid sending multiple requests (one for each name), so first I'm taking all the existing 
            countries (there are not many) to extract their codes and send in a second and final request the list 
            of codes of the countries inserted in the frontend.
         */

        if (!req.query || !req.query.countryNames) {
            return res.status(400).json({ message: "Missing country names" });
        }

        await getAllCountries(req)
            .then((allCountries) => {
                if (allCountries[0]) {
                    const contryNames = req.query.countryNames as string;
                    const countryNamesArr = contryNames.split(';');
                    let countryCodes = '';
                    countryNamesArr.forEach((country) => {
                        const countriesMatched = allCountries.filter(c => c.name.toLowerCase().includes(country.toLowerCase()));
                        const countriesMatchedCodes = countriesMatched.map((c) => { return c.alpha2Code });
                        if (countriesMatchedCodes && countriesMatchedCodes.length > 0) {
                            countryCodes += countriesMatchedCodes.join(';') + ';';
                        }
                    })
                    if (countryCodes == '') {
                        return res.status(404).json({ message: "Not Found" });
                    }
                    getCountriesListByCode(req, countryCodes)
                        .then((allCountriesFiltered) => {
                            return res.json(allCountriesFiltered);
                        })
                        .catch((error) => {
                            return res.status(error.status).json({ message: error.message });
                        });
                } else {
                    return res.status(allCountries.status).json({ message: allCountries.message });
                }
            })
            .catch((error) => {
                return res.status(error.status).json({ message: error.message });
            })
    });


    /**
     * List all the countries on the client and create a field to filter them by name
        ● On the backend, use this API for countries https://restcountries.eu/
     */
    app.get("/countries", async (req, res) => {
        await getAllCountries(req)
            .then((allCountries) => {
                return res.json(allCountries);
            })
            .catch((error) => {
                return res.status(error.status).json({ message: error.message });
            })
    })

}

