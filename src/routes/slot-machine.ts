module.exports.addRoutes = (app) => {

    /**
        Considering a Slot machine defined like this:
        ● Reel1: [“cherry”, ”lemon”, “apple”, ”lemon”, “banana”, “banana”, ”lemon”, ”lemon”]
        ● Reel2: [”lemon”, “apple”, ”lemon”, “lemon”, “cherry”, “apple”, ”banana”, ”lemon”]
        ● Reel3: [”lemon”, “apple”, ”lemon”, “apple”, “cherry”, “lemon”, ”banana”, ”lemon”]
        The user starts with 20 coins. Each spin will cost the user 1 coin.
        Please note that slot machines only consider pairs a match if they are in order from left to
        right.
        Eg:
        Apple, Cherry, Apple - no win
        Apple, Apple, Cherry - win
        Rewards
        ● 3 cherries in a row: 50 coins, 2 cherries in a row: 40 coins
        ● 3 Apples in a row: 20 coins, 2 Apples in a row: 10 coins
        ● 3 Bananas in a row: 15 coins, 2 Bananas in a row: 5 coins
        ● 3 lemons in a row: 3 coins
        Create an endpoint on the backend that when called by the frontend will return the result of the spin
        and the coins the player won.
     */
    app.get("/slot-machine", (req, res) => {

        const reel1 = ["cherry", "lemon", "apple", "lemon", "banana", "banana", "lemon", "lemon"];
        const reel2 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];
        const reel3 = ["lemon", "apple", "lemon", "apple", "cherry", "lemon", "banana", "lemon"];

        const slotMachineMatch = [
            {
                name: "cherrycherrycherry",
                coins: 50
            },
            {
                name: "cherrycherry",
                coins: 40
            },
            {

                name: "appleappleapple",
                coins: 20,
            },
            {

                name: "appleapple",
                coins: 10,
            },
            {
                name: "bananabananabanana",
                coins: 15
            },
            {
                name: "bananabanana",
                coins: 5
            },
            {

                name: "lemonlemonlemon",
                coins: 5,
            }
        ];

        let spinResult = [
            reel1[Math.floor(Math.random() * reel1.length)],
            reel2[Math.floor(Math.random() * reel2.length)],
            reel3[Math.floor(Math.random() * reel3.length)]
        ];

        const spinResultString = spinResult.join().replace(/,/g, '');
        const isMatch = slotMachineMatch.find(s => spinResultString.includes(s.name));
        const coins = isMatch ? isMatch.coins : 0;

        return res.status(200).json({ result: spinResult, coins });
    })
}