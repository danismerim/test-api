const countries = require('./countries');
const users = require('./users');
const slotMachine = require('./slot-machine');

export default (app) => {
    countries.addRoutes(app);
    users.addRoutes(app);
    slotMachine.addRoutes(app);
}

