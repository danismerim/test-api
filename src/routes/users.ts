import { config } from "../config"
const crypto = require("crypto");
const jwt = require('jsonwebtoken');

// Saving user - Testing purposes
let users = [];

module.exports.addRoutes = (app) => {

    /**
     * Create a register form with fields name, email and password. Must have full validation and fields 
     * are required. Send to the back end and save the user info in memory or any database (SQL or NoSQL).
     */
    app.post("/users", (req, res) => {
        const user = req.body;
        if (!user.name || !user.email || !user.password) {
            return res.status(400).json({ message: "Invalid User" });
        }
        if (!users.find(u => u.email === user.email)) {
            user.password = crypto.createHash("sha256").update(user.password).digest("hex");
            users.push(user);
            console.log("####### All users ######")
            console.log(users)
            console.log("#############")
            return res.status(201).json({ message: "User Created" });
        }
        return res.status(409).json({ message: "This user is already registered" });
    })


    /**
     * Using the account info from Question 4, create a login form which returns a JWT token from the 
     * backend and uses that to maintain a session.
     */
    app.post("/login", (req, res) => {
        const user = req.body;
        if (!user.password || !user.email) {
            return res.status(403).json({ message: "Wrong Credentials" });
        }
        user.password = crypto.createHash("sha256").update(user.password).digest("hex");
        const userSaved = users.find(u => u.email === user.email && u.password === user.password);
        if (userSaved) {
            const token = jwt.sign(
                { user: userSaved.name, email: userSaved.email },
                config.token_secret,
                { expiresIn: '1800s' }
            );
            return res.json({ token });
        } else {
            return res.status(403).json({ message: "Wrong Credentials" });
        }
    })
}